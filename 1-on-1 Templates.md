# 1:1 Templates

A collection of thematically-related questions for 1-on-1 meetings. These questions can be added to the agenda beforehand to spark a more interesting conversation than just status updates.

## Annual Goal Setting Conversation

For when HR makes you fill out annual goals and you want them to actually be meaningful.

1. What goals did you set yourself last year? How did you do on those?
2. What are your current strengths and weaknesses, and how do those translate into growth opportunities for this coming year?
3. Do you have a high-level vision for what you want your career to look like 3-5 years from now?
4. What things do you need to get done this year to move yourself measurably closer to that vision?
5. For each of those goals, what's the single next step you need to take? (Think at the level of the next week or two, not the next six months.)

## More Frequent Goals and Career Conversation

A check-in about goals and career progression that should happen more often than anually. Monthly is good, I probably wouldn't go less frequent than quarterly.

1. On a scale from 1-5, where 1 is "I'd have more career prospects at Burger King" and 5 is "I'll be working here when I retire", how are you feeling about your career trajectory?
2. Of the annual goals you set for yourself, which one are you most focused on right now? What's the next step you need to take to make progress towards it?
3. What things can I be doing to support you in making progress towards that goal?
4. How's your work-life balance?
5. What percentage of your time are you spending on [deep vs. shallow work](https://medium.com/@new2code/deep-work-vs-shallow-work-how-to-be-more-focused-da79c0561082)? What percentage would you like to be spending?

## Company Vision and Strategy

A check-in about the company strategy and overall health of the company. Frequency of a conversation like this is up to you, but it's probably good for everybody to have an reasonably up-to-date understanding of this.

1. What's your understanding of the company's current market strategy?
2. How is the company doing overall? How can you tell?
3. What's one thing the company would be crazy _not_ to do in the next 6-12 months?
4. If you became the CEO overnight, what's the first change you would implement?
5. What area of the company would you like to learn more about?

## Team Vision and Strategy

A check-in like the previous one but at the team level rather than the company level.

1. How does our team fit into the larger company strategy?
2. What are our team's current goals? How are we doing on them?
3. What's one thing the team would be crazy _not_ to do in the next 6-12 months?
4. If you became our team lead overnight, what's the first change you would implement?
5. What area of the team codebase would you like to learn more about?

## Day-to-Day, Process, and Pain Points

A conversation to get a sense of the issues people are running into in their day-to-day work. Don't ask about this unless you actually want to help do something about it.

1. What problem are you working on right now?
2. What was the biggest waste of your time this past week?
3. What's one thing we could do to make our process or tooling less obnoxious?
4. How's your life going outside of work?
5. What was your favorite day of work in the last month? Why?

## Autonomy, Mastery, and Purpose

A conversation about people's motivators and demotivators.

1. Are you being challenged at work?
2. Why does your current project matter? How is it important for the team? The company? The customers?
3. How do you feel about the level of direction you're getting from me? Not enough support? Too micro-manage-y?
4. What skills have you been able to develop in the past month?
5. What's something you find particularly motivating? What's something you find really de-motivating?

## Pulse Check

A conversation about how somebody is feeling with respect to their work, the team, and the company. There's some overlap here with the 

1. On a scale of 1 to 5, where 1 is "I am actively looking for other jobs" and 5 is "I can't wait to get up in the morning so that I can start work!" how are you currently feeling about work?
2. How stressful if your job at the moment? Is it the good kind of stress or not?
3. How comfortable are you speaking up in meetings?
4. How connected are you with the rest of the team?
5. How responsive is the company to feedback? How responsive am I?

## Feedback and Coaching Session

1. What's one thing you did recently that you'd like specific feedback on OR what's a problem/skill you'd like coaching with?

