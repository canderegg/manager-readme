# Onboarding Questions for the Initial 1-on-1

Adapted from _The Manager's Path: A Guide for Tech Leaders Navigating Growth and Change_,
by Camille Fournier.

* Why did you decide to work here? What are you excited about?

* Is there a particular area of interest within Computer Science that you love working? Are there areas you haven't had a chance to work but would be excited to learn more about?

* Are you OK with public praise or recognition, or does that make you uncomfortable?

* What is your preferred method of communication for serious feedback? Do you prefer to get such feedback face to face, or would you rather have it in writing so you have time to digest it?

* Any things I should be aware of that you know put you in a bad mood? What does a bad mood look like for you?

* Are there any manager behaviors you know you hate? Are there any that have worked well for you in the past?

* Do you have any clear career goals I should know about so I can help you achieve them?

* Any surprises since you joined, good or bad, that I should know about?

* What do you do for fun outside of work?
