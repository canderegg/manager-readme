# Manager README
written by [Caspar Anderegg](http://casparanderegg.com), Engineering Manager @ [Square](https://squareup.com/us/en)

What is this document? A Manager README, inspired by [this article here](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe)! The goal is to give you an overview of my preferred working style and management outlook. It should help get us up to speed and on the same page.

The goal is to provide context on me, _not_ mandate any particular practices. If there's something here you don't think will work for you, please let me know!

## My Role

My job responsibilities as an Engineering Manager are as follows, in order of importance:

1. Attract, retain, and develop awesome engineers.
2. Give them the tools and context they need to be successful.
3. Ensure that information is flowing freely up, down, and across the org chart.

If you feel like I'm missing the boat on any of these, you'd be doing me a *huge* favor by letting me know. Feel like you're stagnating in what you're doing? Confused about why we're working on project X? Think your feedback on the new company process is getting ignored? Come talk to me about it!

My job is *not* to tell you what to do. In fact, my ideal world is one where I can take a month off without being missed.

## Guiding Principles

### Growth Mindset

I'm a big believer in [Growth Mindset](https://www.mindsetworks.com/science/) - the idea that abilities are mostly developed over time rather than innate. I would much rather have you push yourself, even if it means failing sometimes, than only stick to things you know you can handle.

A couple ideas follow from this principle:

1. It's fine not to understand a particular task, or not to know how to approach a particular problem. I'm happy to help clarify goals or act as a sounding board for ideas.
2. It's **not** fine to believe you will *never* be able to understand the task or solve the problem.

### Optimize for Humans

Most of my leadership biases are people-centric. Informed, happy, engaged engineers build better products. And more importantly, they want to keep doing it for a long time. Other leaders might focus on other areas, and having that diversity of perspectives is valuable, but in most situations I'm trying to figure out how to optimize for the humans involved.

### Start From "Why?"

This one is pretty encoded in Square's culture, but it's worth reiterating. Very few people are engineers just because of the paycheck. "You should work on this thing because we're paying you to," is a terrible justification for a project. You should always understand the *why* behind the work you're doing. How will it impact customers? How will it impact the team? The company? The world at large?

If that *why* hasn't been made clear to you, ask! If nobody can tell you, you should probably be working on something else.

### Empathy as a Priority

One of the biggest values of a diverse team is that they can bring a wide range of ideas, perspectives, and backgrounds to bear on a problem. A lot of times those perspectives are intrinsically tied to their unique identities and experiences of how the world works. There's going to be some friction when those different experiences come up against each other, and that friction is a feature not a bug. Working through problems as a group can at times be messy and uncomfortable, but the alternative is never being challenged on the things you think are true.

In order to make this work, I'm going to ask that you engage with other folks on the team from a place of empathy. Assume that they, like you, are earnestly trying to solve the problem at hand. Try to first understand where they're coming from and what the situation looks like from their perspective, then work from there towards a joint solution.

At the end of the day this is a team of people first, and engineers second.

## Feedback

Nobody is perfect at their job (especially me), there's always room to learn more. One of the best ways to keep learning is through ongoing feedback. This includes feedback from you on what I should be doing better.

There are a couple structured ways to get feedback on a regular cadence (outlined below) but don't feel the need to stick to those structures. If you have feedback for me, or want feedback on something specific, feel free to reach out to me immediately.

### One on Ones

We'll have a 30 minute 1:1 meeting once a week. Generally this will be a video call, unless we happen to be in the same office in which case we'll do it in person. This is not intended to be a status update meeting (though it can include status updates). Rather, this is a great time to ask for feedback on specific things, talk about any issues you're running into (technical or otherwise), and bring up any areas where you think the team can improve.

This is your time, I encourage you to bring a couple questions or topics you want to talk about. If you don't have anything for a particular week, I typically have some questions on my mind so we can talk about those, but don't feel like we need to stick to my agenda.

If for some reason I can't make our regularly scheduled 1:1, I will try to reschedule it rather than canceling. I ask that you do the same.

### "Level-Up" One on Ones

Every six weeks, we'll designate one of our 1:1 meetings as a "level-up". This is a place to talk at about your career goals, long term aspirations, what you need to do to get to the next promotion, etc. The focus here is on leveling up your skills to be better at the work you do, and to make sure you're getting what you want out of working here.

This doesn't mean we shouldn't be having career progression conversations more regularly. We can talk about your career in any 1:1 meeting, level-up or not. It's just easy for these conversations to get pushed off unless there's explicit time for them - so this is the explicit time!

## Other Things About Me

### Introversion

I'm generally pretty introverted. In a group of more than two or three people, I spend a lot more time listening than speaking. Please don't mistake my being quiet for being disinterested. It's just how I process things.

### Work Life Balance

I love my work, but it's not my only passion. I try pretty hard not to work after hours or on weekends, and encourage my team to do the same. My email doesn't go to my phone. When I'm not at work I'm not at work.

I generally start work around 8:30am and finish around 4:30pm Central time (9:30 - 5:30 Eastern) I should be pretty responsive during those hours. If you need to get in touch with me outside those hours a direct message on Slack or a text is your best bet, but if it's something that can wait I may not respond until the next day.

### Don't be Afraid to Bug Me

I have a lot on my plate, occasionally things will fall through the cracks (though I try hard to avoid it). If I promised to look into something and haven't followed up, don't be afraid to bring it up again. Making sure you have what you need is at the top of my priority list.

## Further Resources in this Repo

 - [Onboarding Questions](OnboardingQuestions.md) Some questions that may come up in our first 1:1 meeting.
 - [1:1 Templates](1-on-1 Templates.md) A collection of thematically-related 1:1 questions to help spark interesting discussions.
